# vim:ts=4 sw=4 sts=2 noexpandtab

# Some vars useful later on
SUBUID=$(shell awk -F: '/^dockremap/ {print $$2}' /etc/subuid)
SUBGID=$(shell awk -F: '/^dockremap/ {print $$2}' /etc/subgid)
SMBUID=$(shell awk -F= '/^SMB_UID/ {print $$2}' config.env)
SMBGID=$(shell awk -F= '/^SMB_GID/ {print $$2}' config.env)

DOCKER_BUILDKIT=1

.PHONY: install-cron
install-cron:
	sed 's|{{REPO_PATH}}|$(PWD)|' cron > cron.docker-timemachine
	install --compare --verbose cron.docker-timemachine /etc/cron.d/docker-timemachine

#.PHONY: backup
#backup:
#	docker cp -a timemachine_samba_1:/var/lib/netatalk/ backup/
#	if [ "$(shell git status --porcelain | grep backup | wc -l)" -gt 0 ]; then \
#		git add backup; \
#		git commit -vm "Automated backup on $(shell date -Is)"; \
#	fi

.PHONY: tag
tag:
	docker tag \
		timemachine_samba:latest \
		timemachine_samba:$(shell date +"%Y%m%d_%H%M%S")
	docker tag \
		timemachine_avahi:latest \
		timemachine_avahi:$(shell date +"%Y%m%d_%H%M%S")

.PHONY: buildv
buildv:
	DOCKER_BUILDKIT=1 docker compose \
		build \
		--force-rm \
		--pull \
		--no-cache
	make tag

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker --log-level ERROR compose --ansi never \
		build \
		--force-rm \
		--pull \
		--no-cache \
		--quiet
	make tag

.PHONY: testparm
testparm:
	docker run -t --rm \
		-v $(PWD)/rootfs.samba/etc/samba/smb.conf.d:/etc/samba/smb.conf.d \
		--env-file config.env \
		--entrypoint testparm \
		timemachine_samba:latest \
		-s /etc/samba/smb.conf

.PHONY: test
test: testparm

.PHONY: stop
stop:
	docker compose down

.PHONY: start
start:
	docker compose up -d

.PHONY: restart
restart: stop start

.PHONY: rmi
rmi:
	docker images timemachine_samba --filter "before=timemachine_samba:latest" -q | xargs --no-run-if-empty docker rmi 
	docker images timemachine_avahi --filter "before=timemachine_avahi:latest" -q | xargs --no-run-if-empty docker rmi 

.PHONY: fix-perms
fix-perms:
	find data/var ! -uid $(SUBUID) -exec chown $(SUBUID):$(SUBGID) '{}' \;
	find data/timemachine ! -uid $$(( $(SUBUID) + $(SMBUID) )) -exec chown $$(( $(SUBUID) + $(SMBUID) )):$$(( $(SUBGID) + $(SMBGID) )) '{}' \;

.PHONY: update
update: build restart rmi
