# Timemachine in a docker container using Samba

Sets up a samba based timemachine for network backups.

## Requirements

- Docker Compose V2
- make (optional)

## Overview

Two containers are set up:

1. Samba container behind the usual docker network interface
2. Avahi container on the host network interface to publish the service via Bonjour/Zero-conf

## Quickstart

1. Copy `config-sample.env` to `config.env` and edit with relevant settings.

2. Start up the service:

       make buildv start

## Background

A time machine docker image using samba and avahi. The Samba config is mostly
based on [u/KervyN's
HowTo](https://www.reddit.com/r/homelab/comments/83vkaz/howto_make_time_machine_backups_on_a_samba/)
and [dperson's](https://github.com/dperson) [Samba docker
container](https://github.com/dperson/samba).

## Variables in the config-sample.env

| Varibable    | Description |
| -------------|-------------|
| SMB_USER     | Samba user name for connection.  Only a single user is supported currently. |
| SMB_PASSWORD | Samba user password |
| SMB_UID      | POSIX user ID in the container. Should match your client user ID. |
| SMB_GID      | POSIX group ID in the container. Should match your client group ID. |
| GLOBAL_QUOTA | TimeMachine Size in bytes. See sample file for an example of 2TB.  Must be set on initial launch, and would have to be changed manually thereafter. |

## Extending samba configuration

There are two areas that can be locally customized:

1. The samba configuration
2. The Docker Compose configuraiton

This can be used to make more volumes available, or to tune existing
configuration.

### Samba configuration

You can put additional samba configuration into two additional files.

Test your config files by running `make testparm`.  This does depend on having
previously built an image locally, so you may need to run `make buildv` first.

1. `rootfs.samba/etc/samba/smb.conf.d/timemachine.conf`: config placed in this
   file will be literally added to the main TimeMachine share configuration.

   You might want to add attributes such as `valid user = ...` to limit who can
   access this share.

2. `rootfs.samba/etc/samba/smb.conf.d/local.conf`: config placed in this file
   will literally be included at the end of the `[global]` configuration so it
   can be used to configure additional shares or you can use it to include
   additional config files via smb.conf standard substitution patterns.

### Docker Compose overrides

You can use the usual Compose file configuration override mechansims by
creating a `docker-compose.override.yml` file and setting the values in there.
Details of how Docker Compose merges the values can be found in the [Compose
documentation](https://docs.docker.com/compose/extends/#adding-and-overriding-configuration)

## Example additional share configuration

Here's an example for adding an additional share to the container service.

```ini
# rootfs.samba/etc/samba/smb.conf.d/local.conf
-------------------------------------------------------------------------
[Another Share]
path = /another_share
browsable = yes
read only = yes
valid users = guest
```

Then add the additional share to a `docker-compose.override.yml` file (see
above for details), for example:

```yaml
# docker-compose.override.yml
-------------------------------------------------------------------------
version: "2.4"

services:
 samba:
   volumes:
     - /path/to/another_share:/another_share
```

## Tips

1. When doing the initial backup you can disable throttling on the macOS system
   reduce the duration of the first backup.

       sudo sysctl debug.lowpri_throttle_enabled=0

   Remember to re-enable throttling after the first backup to prevent the
   system from experiencing performance degradations during subsequent backups.

       sudo sysctl debug.lowpri_throttle_enabled=1

1. If the samba host disk volume is on a Copy on Write (CoW) filesystem (e.g.
   BTRFS) you should consider disabling CoW on the data directory.  With CoW
   enabled I've observed corrupted backup sparseimages.  Disabling CoW may help
   with this.

   If you're experiencing problems with the backups the following steps may help:

   1. In the Finder navigate to the samba server share and attempt to mount the
      sparseimage.

      If that succeeds then the disk image is unlikely to be corrupt.

   2. If the sparseimage fails to mount then open up Disk Utility and you
      should find the sparseimage recognized but unmounted.  Run a First Aid
      check against the volume.
