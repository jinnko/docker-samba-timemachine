#!/usr/bin/with-contenv sh

set -eu

if [ -z "$SMB_USER" ]; then
    echo "Set SMB_USER environment variable to continue"
fi

cmd=""
if [ -n "$SMB_UID" ]; then
    cmd="$cmd --uid $SMB_UID"
fi

if ! getent passwd "$SMB_USER" >/dev/null; then
    # shellcheck disable=SC2086
    adduser $cmd -s /sbin/nologin -H -D -g '' "$SMB_USER" || :
fi

if [ -n "$SMB_GID" ]; then
    cmd="$cmd --gid $SMB_GID"
    addgroup -g "$SMB_GID" "$SMB_USER" || :
fi

if [ -n "$SMB_PASSWORD" ]; then
    echo "$SMB_USER:$SMB_PASSWORD" | chpasswd
    printf "%s\n%s" "$SMB_PASSWORD" "$SMB_PASSWORD" | smbpasswd -s -a "$SMB_USER"
    echo "System and smb password set"
fi

if [ ! -d /timemachine ]; then
    echo "/timemachine path doesn't exist."
    exit 1
else
    chown -R "$(awk -F: '/^'"$SMB_USER"'/ {print $3 ":" $4}' /etc/passwd)" /timemachine
fi

if [ ! -r /timemachine/.com.apple.TimeMachine.quota.plist ]; then
  sed "s/##GLOBAL_QUOTA##/$GLOBAL_QUOTA/" /.com.apple.TimeMachine.quota.plist > /timemachine/.com.apple.TimeMachine.quota.plist
  echo "Created global quota config at /timemachine/.com.apple.TimeMachine.quota.plist"
fi

exec /usr/sbin/smbd --foreground --no-process-group --debug-stdout --configfile=/etc/samba/smb.conf
